from pygame import rect
from itertools import product


DANGER = 0
BLOCKED = 1
ENEMY_FOUND = 2
MYSTERY_FOUND = 3
REST = -1

DODGE_BULLET = 100
FIND_ENEMY = 200
FIND_MYSTERY = 300


class GamePercepts:
    """
    ADT for collecting and storing percepts collected during gameplay.
    It also has functions to analyse the percepts collected.
    """

    def __init__(self):
        self.shipPos = rect.Rect(0, 0, 0, 0)
        self.mysteryPos = set()
        self.enemyPos = set()
        self.blockerPos = set()
        self.bulletPos = set()
        self.ship_gun_pos = set()
        self.score = 0

    def ship_can_be_shot(self) -> bool:
        """
        Check if a ship is in a bullet(s) collision path.
        """
        ship_width = 10 + \
            ((self.shipPos.topright[0] - self.shipPos.topleft[0]) // 2)
        danger_coord = range(
            self.shipPos.centerx - ship_width,
            self.shipPos.centerx + ship_width)
        for bullet in self.bulletPos:
            if bullet.rect.centerx in danger_coord and bullet.rect.midbottom[1] > 400:
                return True
        return False

    def ship_has_found_enemy(self) -> bool:
        """
        Check if a bullet shot by ship can hit enemy ship.
        """
        enemy_width = 0
        if self.enemyPos:
            enemy = self.enemyPos.pop()
            self.enemyPos.add(enemy)
            enemy_width = ((enemy.rect.topright[0] -
                            enemy.rect.topleft[0]) // 2) - 10
        for enemy in self.enemyPos:
            shooting_coord = range(
                enemy.rect.centerx - enemy_width,
                enemy.rect.centerx + enemy_width)
            for pos in self.ship_gun_pos:
                if pos in shooting_coord:
                    return True
        return False

    def ship_is_blocked(self) -> bool:
        """
        Check if the whole ship is behind blockers.
        """
        ship_width = (
            (self.shipPos.topright[0] - self.shipPos.topleft[0]) // 2)
        danger_coord = set(range(
            self.shipPos.centerx - ship_width,
            self.shipPos.centerx + ship_width))
        blocker_coord = self.get_blockers()
        for x_coord in danger_coord:
            if not (x_coord in blocker_coord):
                return False
        return True

    def mystery_in_game(self) -> bool:
        """
        Confirm that a mystery is in game window.
        """
        return any(0 < mystery.rect.x < 800 for mystery in self.mysteryPos)

    def __is_blocked(self, enemy) -> bool:
        """
        Check if an enemy's coordinates is behind blockers.
        """
        enemy_width = ((enemy.topright[0] - enemy.topleft[0]) // 2)
        danger_coord = set(range(
            enemy.centerx - enemy_width,
            enemy.centerx + enemy_width))
        blocker_coord = self.get_blockers()
        for x_coord in danger_coord:
            if not (x_coord in blocker_coord):
                return False
        return True

    def get_blockers(self) -> set:
        """
        Get a set of all x-coordinates of blockers.
        """
        blocker_coord = set()
        for blocker in self.blockerPos:
            blockerx = set(
                range(blocker.rect.centerx - 5, blocker.rect.centerx + 5))
            for i in blockerx:
                blocker_coord.add(i)
        return blocker_coord

    def coordinate_is_blocked(self, coord) -> bool:
        """
        Check whether a given x-coordinate is blocked.
        """
        blocker_coord = self.get_blockers()
        if coord in blocker_coord:
            return True
        return False

    def get_closest_enemy(self) -> int:
        """
        Calculate distances between ship and enemies set and return the shortest distance.
        """
        closest_enemy = 999999
        if len(self.enemyPos) == 1:
            enemy = self.enemyPos.pop()
            self.enemyPos.add(enemy)
            for gun in self.ship_gun_pos:
                closest_enemy = min(
                    (gun - enemy.rect.centerx, closest_enemy), key=abs)
        else:
            for enemy in self.enemyPos:
                if not self.coordinate_is_blocked(enemy.rect.centerx):
                    for gun in self.ship_gun_pos:
                        closest_enemy = min(
                            (gun - enemy.rect.centerx, closest_enemy), key=abs)
        return closest_enemy if closest_enemy != 999999 else 0

    def get_closest_bullet(self):
        """
        Calculate distances between ship and bullets and return the shortest distance.
        """
        closest_bullet = 999999
        for bullet in self.bulletPos:
            if not self.__is_blocked(bullet.rect):
                closest_bullet = min(
                    [self.shipPos.centerx - bullet.rect.centerx, closest_bullet], key=abs)
        return closest_bullet

    def get_closest_mystery(self):
        """
        Calculate distances between ship and mystery and return the shortest distance.
        """
        closest_mystery = 999999
        for mystery in self.mysteryPos:
            for gun in self.ship_gun_pos:
                closest_mystery = min(
                    (self.shipPos.centerx - mystery.rect.centerx, closest_mystery), key=abs)
        return closest_mystery

    def ship_has_found_mystery(self):
        """
        Check if a bullet shot by ship can hit mystery ship.
        """
        mystery_width = 0
        if self.mysteryPos:
            mystery = self.mysteryPos.pop()
            self.mysteryPos.add(mystery)
            mystery_width = ((mystery.rect.topright[0] -
                              mystery.rect.topleft[0]) // 2)

        for mystery in self.mysteryPos:
            shooting_coord = range(
                mystery.rect.centerx - mystery_width,
                mystery.rect.centerx + mystery_width)
            for pos in self.ship_gun_pos:
                if pos - mystery_width in shooting_coord:
                    return True
        return False

    def get_stats(self):
        """
        Dump all percepts collected to the console
        """
        print('Ship:', self.shipPos)
        print('Enemies:', self.enemyPos)
        print('Bullets:', self.bulletPos)
        print('Blockers:', self.blockerPos)
        print('Ship can be shot:', self.ship_can_be_shot())
        print('Ship has found enemy:', self.ship_has_found_enemy())
        print('Ship is fully blocked:', self.ship_is_blocked())
        print('Distance to closest enemy:', self.get_closest_enemy())
        print("Mystery:", self.mysteryPos)
        print("Mystery in game:", self.mystery_in_game())
        print("Closest mystery:", self.get_closest_mystery())
        print("Ship has found mystery:", self.ship_has_found_mystery())
        print("Can shoot from:", self.ship_gun_pos)


class State:
    """
    ADT that defines the state of the game.
    """

    def __init__(self, p: GamePercepts):
        self.__percepts = p
        self.__states = list()
        self.__possible_states = {}
        self.__danger_states = list()
        self.__safe_states = list()
        self.__mystery_states = list()
        self.__enemy_states = list()
        self.__blocked_states = list()
        self.__rest_states = list()
        self.graph = dict()
        self.get_all_states()

    def get_state(self) -> int:
        """
        Return a state got from analysing percepts
        """
        if self.__percepts.ship_can_be_shot():
            self.__states.append(DANGER)
        if self.__percepts.ship_is_blocked():
            self.__states.append(BLOCKED)
        if self.__percepts.ship_has_found_enemy():
            self.__states.append(ENEMY_FOUND)
        if self.__percepts.ship_has_found_mystery():
            self.__states.append(MYSTERY_FOUND)
        self.__states = list(set(self.__states))
        for key, value in self.__possible_states.items():
            if sorted(self.__states) == sorted(value):
                self.__states.clear()
                return key
        self.__states.clear()
        return REST

    def get_all_states(self):
        """
        Compute all states as a cartesian product of {DANGER,BLOCKED,ENEMY_FOUND,MYSTERY_FOUND}
        """
        possibilities = (
            DANGER,
            BLOCKED,
            ENEMY_FOUND,
            MYSTERY_FOUND,)
        # Store the results in a list to prevent random storage
        temp_state_list = list()
        for state in list(product(possibilities,
                                  repeat=len(possibilities))):
            s = set(state)
            if s not in temp_state_list:
                temp_state_list.append(set(state))
        index = 1
        for i in temp_state_list:
            self.__possible_states[index] = list(i)
            if DANGER in i:
                self.__danger_states.append(i)
            else:
                self.__safe_states.append(i)
            if MYSTERY_FOUND in i:
                self.__mystery_states.append(i)
            if ENEMY_FOUND in i:
                self.__enemy_states.append(i)
            if BLOCKED in i:
                self.__blocked_states.append(i)
            if REST in i:
                self.__rest_states.append(i)
            index += 1

    def get_target_states(self) -> list:
        state_list = list(self.__possible_states.keys())
        return [state_list[-1], state_list[-3]]

    def evaluate_state(self, state: int) -> int:
        if state == -1:
            return []
        return self.__possible_states.get(state)

    def percept_to_state(self, state) -> int:
        for key, value in self.__possible_states.items():
            if sorted(state) == sorted(value):
                return key
        return -1

    def generate_graph(self) -> dict:
        """
        Build and return an adjacency list for all states
        """
        for key, value in self.__possible_states.items():
            if set(value) in self.__danger_states:
                self.graph[key] = [self.percept_to_state(
                    state) for state in self.__safe_states if key != self.percept_to_state(state)]
            if set(value) in self.__safe_states:
                if set(value) in self.__blocked_states:
                    self.graph[key] = [self.percept_to_state(
                        state) for state in self.__enemy_states if (
                            key != self.percept_to_state(state) and state in self.__safe_states)]
                    self.graph[key].extend([self.percept_to_state(
                        state) for state in self.__mystery_states if (
                            key != self.percept_to_state(state) and state in self.__safe_states)])
                if set(value) in self.__enemy_states:
                    self.graph[key] = [self.percept_to_state(
                        state) for state in self.__mystery_states if (
                            key != self.percept_to_state(state) and state in self.__safe_states)]
        self.graph[REST] = self.get_target_states()
        print(self.graph)
        return self.graph

    def change_state(self, states: tuple) -> int:
        """
        Jump from one state to another by performing actions
        """
        state_evaluation = self.evaluate_state(
            states[0]) if states[0] != -1 else self.evaluate_state(states[1])
        if state_evaluation and set(state_evaluation) in self.__danger_states:
            return DODGE_BULLET
        if state_evaluation and set(state_evaluation) in self.__mystery_states:
            return FIND_MYSTERY
        if state_evaluation and set(state_evaluation) in self.__enemy_states:
            return FIND_ENEMY
        if set(state_evaluation) in self.__blocked_states:
            if self.__percepts.mystery_in_game():
                return FIND_MYSTERY
            return FIND_ENEMY
