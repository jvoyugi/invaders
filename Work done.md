# Space invaders game

#### Aim

* To make the ship in space invaders game intelligent

#### How to run

* You need to make sure you have [Python3](https://www.python.org/downloads/) and [Pygame](http://www.pygame.org/download.shtml) installed.

* Then install [Networkx](https://networkx.github.io/):

```bash
pip3 install networkx
```

* If you have the correct version of Python and Pygame installed, you can run the program in the command prompt / terminal:

```bash
python spaceinvaders.py
```

**MacOS Mojave**: You need to use Python 3.7.2 or greater: [Source](https://github.com/pygame/pygame/issues/555)

#### What was done

Two classes were made, one for percepts( `utils.GamePercepts` ) and another for states( `utils.State` ).

##### 1. Percepts

This was done by creating a `GamePercepts` class in `utils.py` file. The class is responsible for managing the following information:

1\. `shipPos` - Data about the location of the ship in the game window\.

2\. `mysteryPos` - `set` containing all `Mystery` objects in the game\.

3\. `enemyPos` - `set` containing all `Enemy` objects in the game\.

4\. `blockerPos` - `set` containing all `Blocker` objects in the game\.

5\. `bulletPos` - `set` containing all `Bullet` objects in the game\.

6\. `ship_gun_pos` - `set` containing the location of x-coordinates where shots are fired from\.

7\. `score` - The current score in the game\.

That data is then analysed by the following methods:

1\. `ship_can_be_shot` - Checks if a ship is in the collision path of a bullet\. It does this by taking the ship's x-coordinates from edge to edge and compares it against the center x-coodinates in the `bulletPos` set\. If a bullet exists in the set that matches and the height of that bullet is 400 from the top it return `True` , otherwise it returns `False` \.

2\. `ship_has_found_enemy` - Checks if an enemy is in the collision path of a bullet\. It works in a similar way to `ship_can_be_shot` \. The only difference is that it compares enemy's x-coordinates against `ship_gun_pos` \.

3\. `ship_has_found_mystery` - Checks if a bullet shot by a ship can hit the mystery\. Works in a similar way as `ship_has_found_enemy` \.

4\. `ship_is_blocked` - Checks whether the entire `Ship` is behind blockers\.

5\. `mystery_in_game` - Checks whether a `Mystery` is in the game window\.

6\. `__is_blocked` - Checks whether the entire `Enemy` is behind blockers\.

7\. `coordinate_is_blocked` - Checks whether a given x-coordinate is inside among `blockerPos` set\.

8\. `get_closest_enemy` - Evaluate and return the distance to closest `Enemy` excluding those that are behind blockers except if there is one enemy left in the game\.

9\. `get_closest_bullet` - Evaluate and return the distance to the closest enemy `Bullet` \.

10\. `get_closest_mystery` - Evaluate and return the distance to the closest `Mystery` \.

11\. `get_stats` - Displays all percepts collected in the game by dumping them to the terminal \.

##### 2. State

This was done by creating a `State` class in `utils.py` file. The class is responsible for managing the following information:

1. `percepts` - `GamePercepts` instance used define the state if the game.

2. `states` - A `set` containing all states in the game. It has at most `len(possibilities)` items defined in the `get_all_states` method.

3. `possible_states` - A `dict` containing nodes mapped to states in the game. Nodes act as keys of the dictionary and `states` act as the values of those keys. It has at most `2**len(possibilities) - 1` items.

4. `danger_states` - A `list` containing all `states` that have `DANGER` variable in them. This signals that the `Ship` is in potential danger of being shot.

5. `mystery_states` - A `list` containing all `states` that have `MYSTERY_FOUND` variable. This signals that `Mystery` is in the game.

6. `enemy_states` - A `list` containing all `states` that have `ENEMY_FOUND` variable. This signals that `Enemy` is in the game.

7. `blocked_states` - A `list` containing all `states` that have `BLOCKED` variable. This signifies that a the ship is blocked hence can not shoot.

8. `graph` - An adjacency list formed by linking states in the game.

That data is then analysed by the following methods:

1. `get_state` - Populate `states` with data then compare it with values in `possible_states` the return the key that match. If none match, return `REST` . Just before returning, `states` is emptied since it has become stale.

2. `get_all_states` - Computes the cartesian product of states then assign them to respective keys of `possible_states` .

3. `get_target_states` - Return the keys of `possible_states` such that:
    * A `states` does not have `BLOCKED` .

    * A `states` has either `MYSTERY_FOUND` and/or `ENEMY_FOUND` .

    * A `states` does not have `DANGER` .

4. `evaluate_state` - Return the value of `possible_states` whose key match the state passed.

5. `percept_to_state` - Return the key of the states iterable entered.

6. `generate_graph` - Builds and returns and adjacency list from `possible_states` following these rules:

   * If that value has `DANGER` connect it to those in `safe_states` .

   * If that value is in `safe_states` and `blocked_states` , connect it to `mystery_states` and `enemy_states` .

   * If that value is in `enemy_states` connect it to `mystery_states` .

   * Finally connect `REST` to `get_target_states` .

7. `change_state` - Takes in a `tuple` and returns an `int` representing action to be done.

##### 3. Gameplay

The game is a clone of Space invaders game by Lee Robinson. The repository can be found [here](https://github.com/leerob/Space_Invaders).

`GamePercepts` instance in created. Only one instance is created and used in the game.
A graph is then created. This operation is very expensive, therefore, it is only done once.

Here is the graph it produces:

![graph](./mer.png)

The `Ship` class has additional methods:

1. `dodge_bullet` - This method gets closest bullet, then, if the distance to the bullet is more than 0, it returns 1, else -1. If the x-coordinate of the ship is more than or equal to 650 regardless of distance it returns -1. It is aimed at getting away from the collision path of a bullet.

2. `find_enemy` and `find_mystery` - These methods locate `Enemy` and `Mystery` instances repectively. They do this by calling the `get_closest_enemy` and `get_closest_mystery` methods of the `utils.GamePercept` instance.

3. `do` - This function is called to determine which method is supposed to be executed.

4. `agentProgram` - This is the main agent program of the `Ship` . It takes the state then maps a path using Dijkstra's algorithm from the current state to the goal state. This returns a list where the first course of action is considered. That action passed to `utils.State.change_state` to determine what to do. After that, the action is passed to the `do` method and a result of -1, 0 or 1 is returned which is the direction that the `Ship` should go.

The `SpaceInvaders` class also has the following additional methods:

1. `set_ship_gun_positions` - Appends items to the `utils.GamePercepts.ship_gun_pos` percept.

2. `ship_can_shoot` - Determines if a ship is in a position to shoot.

3. `make_ship_shoot` - It is a modification of `SpaceInvaders.check_input` method which determines wheher the `Ship` object is in prime state to shoot. If so it shoots. I also handles keyboard input, for example, pressing `esc` key exits the game.

* Cleanup operations have been added to `SpaceInvaders.check_collisions` method to remove items from percepts.

* Other percepts are collected at lines 67, 471, 496, 622, 632 and 649
